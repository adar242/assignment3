#ifndef BGS_ENC_DEC
#define BGS_ENC_DEC

#include <BGSMessage.h>
#include <string>
#include <vector>
using std::string;

enum string_code{
    REGISTER,
    LOGIN,
    LOGOUT,
    FOLLOW,
    POST,
    PM,
    USERLIST,
    STAT
};

class BGSencdec{
    protected:
    virtual short bytesToShort(std::vector<char>);
    short opCode;


    private:

    string_code commandOf(string);
    void shortToBytes(short, char*,size_t);

    bool decodingMessage;
    std::vector<char> bytes;
    BGSencdec* currentDecoder;

    public:
    BGSencdec();
    BGSencdec(const BGSencdec& other);
    virtual short getOpcode();
    size_t encode(string,char*);

    virtual ~BGSencdec();
    BGSencdec& operator=(const BGSencdec& other);

    virtual BGSMessage* decodeNextByte(char*);
};

class notifcDecoder : public BGSencdec{
    public:
    notifcDecoder();

    virtual ~notifcDecoder();

    BGSMessage* decodeNextByte(char*);

    private:
    bool gotType;
    char type;
    bool gotUser;
    std::string username;
    std::string content;

};



class errorDecoder : public BGSencdec{
    public:
    errorDecoder();

    virtual ~errorDecoder();

    BGSMessage* decodeNextByte(char*);

    private:
    char firstbyte;
    
};

class ackDecoder : public BGSencdec{
    public:
    ackDecoder();

    virtual ~ackDecoder();

    ackDecoder(const ackDecoder& other);

    ackDecoder& operator=(const ackDecoder& other);

    BGSMessage* decodeNextByte(char*);

    private:
    char firstbyte;
    ackDecoder* currentAckDecoder;

    protected:
    short messageOp;
};

class FollowAckDecoder : public ackDecoder{
    public:
    FollowAckDecoder(int);

    virtual ~FollowAckDecoder();

    BGSMessage* decodeNextByte(char*);

    private:
    short numOfUsers;
    char firstNumByte;
    std::vector<string> usernames;
    std::string username;
    int counter;
    int messageOp;

};

class StatAckDecoder : public ackDecoder{
    public:
    StatAckDecoder();

    virtual ~StatAckDecoder();

    BGSMessage* decodeNextByte(char*);

    private:
    short numOfPosts;
    short numFollowers;
    short numFollowing;
    std::vector<char> bytes;



};



#endif

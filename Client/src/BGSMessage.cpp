#include <BGSMessage.h>

//BGMessage*****************************************************************************************

BGSMessage::BGSMessage(short opcode) : opcode(opcode) {}

short BGSMessage::getOpcode() const { return opcode; }

BGSMessage::~BGSMessage() {

}

//ErrorMessage*****************************************************************************************

ErrorMessage::ErrorMessage(short messageOpcode) : BGSMessage(11), messageOpcode(messageOpcode) {}

short ErrorMessage::getMessageOpcode() const { return messageOpcode; }

ErrorMessage::~ErrorMessage() {

}

//NotificationMessage*****************************************************************************************

NotificationMessage::NotificationMessage(char type, const string &postingUser, const string &content)
        : BGSMessage(9), type(type), postingUser(postingUser), content(content) {}

char NotificationMessage::getType() const {
    return type;
}

const string &NotificationMessage::getPostingUser() const {
    return postingUser;
}

const string &NotificationMessage::getContent() const {
    return content;
}

NotificationMessage::~NotificationMessage() {

}

//AckMessage*****************************************************************************************

AckMessage::AckMessage(short messageOpcode) : BGSMessage(10), messageOpcode(messageOpcode) {}

short AckMessage::getMessageOpcode() const { return messageOpcode; }

AckMessage::~AckMessage() {

}

//StatAckMessage*****************************************************************************************

StatAckMessage::StatAckMessage(short numOfPosts, short numOfFollowers, short numOfFollowing)
: AckMessage(8), numOfPosts(numOfPosts), numOfFollowers(numOfFollowers), numOfFollowing(numOfFollowing) {}

short StatAckMessage::getNumOfPosts() const { return numOfPosts; }

short StatAckMessage::getNumOfFollowers() const { return numOfFollowers; }

short StatAckMessage::getNumOfFollowing() const { return numOfFollowing; }

StatAckMessage::~StatAckMessage() {

}

//UserlistOrFollowAckMessage*****************************************************************************

UserlistOrFollowAckMessage::UserlistOrFollowAckMessage(short messageOpcode, short numOfUsers,
                                                       const vector<string> &usernameVector)
                                                       : AckMessage(messageOpcode), numOfUsers(numOfUsers),
                                                       usernameVector(usernameVector) {}

short UserlistOrFollowAckMessage::getNumOfUsers() const { return numOfUsers; }

const vector<string> &UserlistOrFollowAckMessage::getUsernameVector() const { return usernameVector; }

UserlistOrFollowAckMessage::~UserlistOrFollowAckMessage() {

}

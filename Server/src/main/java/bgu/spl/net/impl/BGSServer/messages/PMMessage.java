package bgu.spl.net.impl.BGSServer.messages;

public class PMMessage extends BGSMessage {

    private String username;
    private String content;

    public PMMessage() {
        super((short)6);
        username = null;
        content = null;
    }

    public String getUsername() {
        return username;
    }

    public String getContent() {
        return content;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setContent(String content) {
        this.content = content;
    }
}

package bgu.spl.net.impl.BGSServer;

import bgu.spl.net.impl.BGSServer.messages.BGSMessage;
import bgu.spl.net.impl.BGSServer.messages.PMMessage;
import bgu.spl.net.impl.BGSServer.messages.PostMessage;

import java.util.*;
import java.util.concurrent.LinkedBlockingQueue;

public class BGSDataBase {

    private HashMap<String, Integer> usernameToId; //holds for each username its connectionId
    private HashMap<String, String> usernameToPassword; //holds for each username its password
    private HashMap<String, List<String>> usernameToFollow; //holds for each username a list of usernames that he follows
    private HashMap<String, List<String>> followToUsername; //holds for each username a list of usernames that follow him
    private HashMap<String, Boolean> userStatus; //holds for each username a Boolean representing whether
    private HashMap<String, List<BGSMessage>> usernameToPostPM; //holds for each username a list of his PMs and posts
    private HashMap<String, Queue<BGSMessage>> waitingMessages; //holds all the messages for a user while he's logged out

    public BGSDataBase() {
        usernameToId = new HashMap<>();
        usernameToPassword = new HashMap<>();
        usernameToFollow = new HashMap<>();
        followToUsername = new HashMap<>();
        userStatus = new HashMap<>();
        usernameToPostPM = new HashMap<>();
        waitingMessages = new HashMap<>();
    }

    public short getNumOfFollowers(String username) {
        return (short)followToUsername.get(username).size();
    }

    public short getNumOfFollowing(String username) {
        return (short)usernameToFollow.get(username).size();
    }

    public List<BGSMessage> getClientMessages(String username) {
        return usernameToPostPM.get(username);
    }

    public Set<String> getUsernameSet() {
        return usernameToPassword.keySet();
    }

    public boolean isRegistered(String username) {
        return usernameToPassword.containsKey(username);
    }

    public boolean isLoggedIn(String username) {
        if (!userStatus.containsKey(username)) return false;
        return userStatus.get(username);
    }

    public int getId(String username) {
        return usernameToId.get(username);
    }

    public void addWaitingMessage(String username, BGSMessage message) {
        waitingMessages.get(username).add(message);
    }

    public void addPostOrPM(String username, BGSMessage message) {
        synchronized (usernameToPostPM) {
            if (message instanceof PMMessage || message instanceof PostMessage)
                usernameToPostPM.get(username).add(message);
        }
    }

    public List<String> getFollowers(String username) {
        return followToUsername.get(username);
    }

    public List<String> getFollowing(String username) {
        return usernameToFollow.get(username);
    }

    public void addFollow(String username, String differentUser) {
        followToUsername.get(differentUser).add(username);
        usernameToFollow.get(username).add(differentUser);
    }

    public void unfollow(String username, String differentUser) {
        followToUsername.get(differentUser).remove(username);
        usernameToFollow.get(username).remove(differentUser);
    }

    public boolean isFollowed(String username, String differentUser) {
        return followToUsername.get(differentUser).contains(username);
    }

    public void login(String username) {
        userStatus.replace(username, true);
    }

    public void logout(String username) {
        userStatus.replace(username, false);
    }

    public boolean passwordMatches(String username, String password) {
        return usernameToPassword.get(username).equals(password);
    }

    public Queue<BGSMessage> getWaitingMessages(String username) {
        return waitingMessages.get(username);
    }

    public void register(String username, String password, int connectionId) {
        usernameToPassword.put(username,password);  //initializing all the hashMaps for this user
        usernameToFollow.put(username, new ArrayList<>());
        followToUsername.put(username, new ArrayList<>());
        usernameToPostPM.put(username, new ArrayList<>());
        usernameToId.put(username, connectionId);
        waitingMessages.put(username, new LinkedBlockingQueue<>());
        userStatus.put(username, false);
    }
}

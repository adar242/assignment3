package bgu.spl.net.impl.BGSServer.messages;

public class StatMessage extends BGSMessage {

    private String username;

    public StatMessage() {
        super((short)8);
        username = null;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}

package bgu.spl.net.impl.BGSServer;

import bgu.spl.net.srv.Server;

public class TPCMain {

    public static void main(String[] args) {
        if (args.length != 1) {
            System.out.println("wrong amount of arguments");
            return;
        }
        BGSDataBase dataBase = new BGSDataBase();
        Server.threadPerClient(Integer.parseInt(args[0]),
                ()->new BidiMessagingProtocolImpl(dataBase),
                ()->new BGSEncDec()).serve();
    }
}


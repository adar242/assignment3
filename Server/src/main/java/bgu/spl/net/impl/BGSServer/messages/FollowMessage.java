package bgu.spl.net.impl.BGSServer.messages;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;

public class FollowMessage extends BGSMessage {

    private char shouldFollow;
    private short numOfUsers;
    private ConcurrentLinkedQueue<String> userNameList;

    public FollowMessage() {
        super((short)4);
        shouldFollow = 0; // waiting to be set as the message is read
        numOfUsers = -1; // waiting to be set as the message is read
        userNameList = new ConcurrentLinkedQueue<>();
    }

    public char getShouldFollow() {
        return shouldFollow;
    }

    public short getNumOfUsers() {
        return numOfUsers;
    }

    public ConcurrentLinkedQueue<String> getUserNameList() {
        return userNameList;
    }

    public void setShouldFollow(char shouldFollow) {
        this.shouldFollow = shouldFollow;
    }

    public void setNumOfUsers(short numOfUsers) {
        this.numOfUsers = numOfUsers;
    }

    public void setAndArrangeUserNameList(String usernames) {
        String[] splitUsernames = usernames.split("0");
        for (String user : splitUsernames)
            userNameList.add(user);
    }
}

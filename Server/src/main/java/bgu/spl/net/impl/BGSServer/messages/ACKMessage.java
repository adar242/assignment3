package bgu.spl.net.impl.BGSServer.messages;

public class ACKMessage extends BGSMessage {

    private short messageOpcode;


    public ACKMessage() {
        super((short)10);
        messageOpcode = 0;
    }

    public ACKMessage(short messageOpcode) {
        super((short)10);
        this.messageOpcode = messageOpcode;
    }

    public short getMessageOpcode() {
        return messageOpcode;
    }


    public void setMessageOpcode(short messageOpcode) {
        this.messageOpcode = messageOpcode;
    }

}

package bgu.spl.net.impl.BGSServer.messages;

public class ErrorMessage extends BGSMessage {

    private short messageOpcode;

    public ErrorMessage() {
        super((short)11);
        messageOpcode = 0;
    }

    public ErrorMessage(short messageOpcode) {
        super((short)11);
        this.messageOpcode = messageOpcode;
    }

    public short getMessageOpcode() {
        return messageOpcode;
    }

    public void setMessageOpcode(short messageOpcode) {
        this.messageOpcode = messageOpcode;
    }
}

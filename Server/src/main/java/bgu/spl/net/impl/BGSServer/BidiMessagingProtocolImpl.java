package bgu.spl.net.impl.BGSServer;

import bgu.spl.net.api.bidi.BidiMessagingProtocol;
import bgu.spl.net.api.bidi.Connections;
import bgu.spl.net.impl.BGSServer.messages.*;
import bgu.spl.net.impl.BGSServer.messages.ackMessages.StatAckMessage;
import bgu.spl.net.impl.BGSServer.messages.ackMessages.UserlistOrFollowAckMessage;

import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;

public class BidiMessagingProtocolImpl implements BidiMessagingProtocol<BGSMessage> {

    private int connectionId;
    private boolean toTerminate;
    private Connections connections;
    private String client_username;
    private BGSDataBase dataBase;

    public BidiMessagingProtocolImpl(BGSDataBase dataBase) {
        toTerminate = false;
        this.dataBase = dataBase;
    }

    @Override
    public void start(int connectionId, Connections<BGSMessage> connections) {
        this.connectionId = connectionId;
        this.connections = connections;
    }

    @Override
    public void process(BGSMessage message) {
        short opCode = message.getOpcode();
        BGSMessage returnMessage; //the error or ack message which will be sent back to the user
        switch (opCode) { // handles each message according to its kind

            case 1:
                returnMessage = processRegister((RegisterMessage)message);
                break;

            case 2:
                returnMessage = processLogin((LoginMessage)message);
                break;

            case 3:
                returnMessage = processLogout((LogoutMessage)message);
                break;

            case 4:
                returnMessage = processFollow((FollowMessage)message);
                break;

            case 5:
                returnMessage = processPost((PostMessage)message);
                break;

            case 6:
                returnMessage = processPM((PMMessage)message);
                break;

            case 7:
                returnMessage = processUserList((UserListMessage)message);
                break;

            case 8:
                returnMessage = processStat((StatMessage)message);
                break;

            default: //the given message is not a Client-to-Server message
                System.out.println("a Server-to-Client message has reached to BidiMessagingProtocolImpl");
                return;
        }
        connections.send(connectionId, returnMessage);
    }

    private BGSMessage processStat(StatMessage message) {
        if (dataBase.isLoggedIn(message.getUsername())) {
            short numOfPosts = 0;
            short numOfFollowers = dataBase.getNumOfFollowers(client_username);
            short numOfFollowing = dataBase.getNumOfFollowing(client_username);
            List<BGSMessage> clientMessages = dataBase.getClientMessages(client_username);
            for (BGSMessage sendMessage : clientMessages) { //checks how many of the client's messages are posts
                if (sendMessage instanceof PostMessage)
                    numOfPosts++;
            }
            return new StatAckMessage(message.getOpcode(), numOfPosts, numOfFollowers, numOfFollowing);
        }
        return new ErrorMessage(message.getOpcode());
    }

    private BGSMessage processUserList(UserListMessage message) {
        if (client_username != null && dataBase.isLoggedIn(client_username)) {
            Set<String> keySet = dataBase.getUsernameSet();
            ConcurrentLinkedQueue userList = new ConcurrentLinkedQueue(keySet); //takes a set of usernames and arranges them in a list
            return new UserlistOrFollowAckMessage(message.getOpcode(), (short)userList.size(), userList);
        }
        return new ErrorMessage(message.getOpcode());
    }

    private BGSMessage processPM(PMMessage message) {
        if (dataBase.isLoggedIn(client_username)) {
            String receiver = message.getUsername();
            if (dataBase.isRegistered(receiver)) { //makes sure the receiver is registered
                if (dataBase.isLoggedIn(receiver)) { //checks if the receiver of this message is logged in
                    NotificationMessage userNotification = postOrPmNotificationCreator(message);
                    connections.send(dataBase.getId(receiver), userNotification);
                }
                else //if he's not logged in the message is inserted to his message queue
                    dataBase.addWaitingMessage(receiver,message);
                dataBase.addPostOrPM(client_username,message);
                return new ACKMessage(message.getOpcode());
            }
        }
        return new ErrorMessage(message.getOpcode());
    }

    private BGSMessage processPost(PostMessage message) {
        if (dataBase.isLoggedIn(client_username)) {
            String content = message.getContent();
            dataBase.addPostOrPM(client_username,message);
            for (String user : dataBase.getFollowers(client_username)) { //for each user that follows this client
                NotificationMessage userNotification = postOrPmNotificationCreator(message);
                connections.send(dataBase.getId(user), userNotification);
            }
            postToTaggedUsers(content, message);
            return new ACKMessage(message.getOpcode());
        }
        return new ErrorMessage(message.getOpcode());
    }

    private void postToTaggedUsers(String content, PostMessage message) { //sends the message to all the tagged users
        boolean noMoreUsers = false; //checks that there are no more users tagged
        int locationOfUsername = content.indexOf("@");
         if (locationOfUsername != -1) //checks if there are any tagged usernames
        while (!noMoreUsers) {
            int endOfUsername = content.indexOf(" ",locationOfUsername);
            String username;
            if (endOfUsername != -1) //if the tagged person is not the last word
                username = content.substring(locationOfUsername+1,endOfUsername);
            else
                username = content.substring(locationOfUsername+1);
            if (dataBase.isRegistered(username)) { //makes sure username is registered
                NotificationMessage userNotification = postOrPmNotificationCreator(message);
                if (dataBase.isLoggedIn(username)) { //if the tagged user is logged in the message is sent immediately
                    connections.send(dataBase.getId(username), userNotification);
                } else { //otherwise the message is inserted to his message queue
                    dataBase.addWaitingMessage(username, userNotification);
                }
            }
            locationOfUsername = content.indexOf("@",locationOfUsername+1);
            noMoreUsers = locationOfUsername == -1;
        }
    }

    private NotificationMessage postOrPmNotificationCreator(BGSMessage message) {
        if (message.getOpcode() == 5) //if it's a post message
            return new NotificationMessage((char) 1, client_username, ((PostMessage) message).getContent());
        else //if it's a PM message
        return new NotificationMessage((char) 0, client_username, ((PMMessage) message).getContent());
    }


    private BGSMessage processFollow(FollowMessage message) {
        if (dataBase.isLoggedIn(client_username)) {
            short successCounter = 0; //how many users the client want to follow/unfollow
            ConcurrentLinkedQueue<String> messageList = message.getUserNameList();
            for (String user: messageList) {
                if (message.getShouldFollow() == '0') { //the message is a follow messages
                    synchronized (dataBase) {
                        if (!dataBase.isFollowed(client_username, user)) {
                            dataBase.addFollow(client_username, user);
                            successCounter++;
                        } else
                            messageList.remove(user);
                    }
                } else { // the message is an unfollow message
                    synchronized (dataBase) {
                        if (dataBase.isFollowed(client_username, user)) {
                            dataBase.unfollow(client_username, user);
                            successCounter++;
                        } else
                            messageList.remove(user);
                    }
                }
            }
            if (successCounter != 0) {
                return new UserlistOrFollowAckMessage(message.getOpcode(), successCounter, messageList);
            }
        }
        return new ErrorMessage(message.getOpcode());
    }

    private BGSMessage processLogout(LogoutMessage message) {
        if (client_username == null){  //user is not logged in, sends error message
            return new ErrorMessage(message.getOpcode());
        }
        synchronized (dataBase) {
            if (dataBase.isLoggedIn(client_username)) { //if the client is logged in
                dataBase.logout(client_username);
                connections.disconnect(connectionId);
                client_username = null;
                return new ACKMessage(message.getOpcode());
            }
        }
        return new ErrorMessage(message.getOpcode());
    }

    private BGSMessage processLogin(LoginMessage message) {
        synchronized (dataBase) {
        if (dataBase.isRegistered(message.getUsername()) && !dataBase.isLoggedIn(message.getUsername())) {
            String password = message.getPassword();
            if (dataBase.passwordMatches(message.getUsername(), password)) {
                client_username = message.getUsername();
                dataBase.login(client_username);
                Queue<BGSMessage> clientWaitingMessages = dataBase.getWaitingMessages(client_username);
                while (!clientWaitingMessages.isEmpty()) //sends all the messages the user got while logged out in order
                    connections.send(connectionId, clientWaitingMessages.remove());
                return new ACKMessage(message.getOpcode());
            }
        }
        }
        return new ErrorMessage(message.getOpcode());
    }

    private BGSMessage processRegister(RegisterMessage message) {
        synchronized (dataBase) {
            if (dataBase.isRegistered(message.getUsername())) //checks if the user is already registered
                return new ErrorMessage(message.getOpcode());
            dataBase.register(message.getUsername(), message.getPassword(), connectionId);
            return new ACKMessage(message.getOpcode());
        }
    }


    @Override
    public boolean shouldTerminate() {
        return toTerminate;
    }
}

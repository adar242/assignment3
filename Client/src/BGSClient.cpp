#include <stdlib.h>
#include <connectionHandler.h>
#include <BGSencdec.h>
#include <messagingProtocol.h>
#include <keyboardReader.h>
#include <thread>
#include <boost/thread.hpp>

using namespace std;

int main (int argc, char *argv[]) {
    if (argc < 3) {
        std::cerr << "Usage: " << argv[0] << " host port" << std::endl << std::endl;
        return -1;
    }
    std::string host = argv[1];
    short port = atoi(argv[2]);
    
    ConnectionHandler connectionHandler(host, port);
    if (!connectionHandler.connect()) {
        std::cerr << "Cannot connect to " << host << ":" << port << std::endl;
        return 1;
    }
    BGSencdec* encdec = new BGSencdec();
    MessagingProtocol* protocol = new MessagingProtocol();
    keyboardReader reader(&connectionHandler,encdec);
    boost::thread readerThread(&keyboardReader::run, &reader);
	

    char *workingByte;
    while (!protocol->shouldTerminate() && (connectionHandler.getBytes(workingByte,1))){
        BGSMessage* message = encdec -> decodeNextByte(workingByte);
        if (message == nullptr){ //still not done reading message
            continue; //go back to start
        }
        else { //done reading message
            protocol -> process(*message);
            delete encdec;
            encdec = new BGSencdec; //reinitialise to easily clean all fields
        }
    }
    readerThread.interrupt();
    connectionHandler.close();
       
    return 0;
}

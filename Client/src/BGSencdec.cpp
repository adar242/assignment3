#include <BGSencdec.h>
#include <sstream>
#include <string>
#include <vector>
#include <iostream>
#include <locale>

using std::string;
using std::stringstream;
using std::vector;

//copy sontructor
BGSencdec::BGSencdec(const BGSencdec& other):opCode(other.opCode),decodingMessage(other.decodingMessage),bytes(other.bytes),currentDecoder(nullptr){
    currentDecoder = new BGSencdec(*other.currentDecoder);
}
//assignment operator
BGSencdec& BGSencdec::operator=(const BGSencdec &other) {
    if (this == &other)
        return *this;
    opCode = other.opCode;
    decodingMessage = other.decodingMessage;
    bytes = other.bytes;
    delete currentDecoder;
    currentDecoder = new BGSencdec(*other.currentDecoder);
    return *this;
}
//default constructor
BGSencdec::BGSencdec():opCode(0),decodingMessage(false), bytes(), currentDecoder(nullptr){}

short BGSencdec::getOpcode(){
    return opCode;
}

//encods the string toEncode to bytes. the encoded bytes will be put in theBytes, and the size of total bytes will return
size_t BGSencdec::encode(string toEncode,char* theBytes){
    string command;
    stringstream ssin(toEncode);
    ssin >> command;

    switch(commandOf(command)){
        case REGISTER:
        {
            string username;
            string password;

            ssin >> username;
            ssin >> password;

            std::vector<char> userBytes (username.begin(), username.end());
            std::vector<char> passwordBytes (password.begin(), password.end());

            size_t bytesLength = 2 + userBytes.size() + 1 + passwordBytes.size() + 1; // op code, bytes for name, 0 byte, byte for password, 0 byte
            size_t bytesIndex = 0;

            short opCode = 1;

            shortToBytes(opCode,theBytes,0); //enter op code to first 2 bytes
            bytesIndex += 2;

            
            for(size_t i = 0; i < userBytes.size(); i++) //getting bytes for username
            {
                theBytes[bytesIndex] = userBytes.at(i);
                bytesIndex ++;
            }

            theBytes[bytesIndex] = char(0); //zero byte
            bytesIndex++;

            
            for(size_t i = 0; i < passwordBytes.size(); i++) //bytes for password
            {
                theBytes[bytesIndex] = passwordBytes.at(i);
                bytesIndex++;
            }

            theBytes[bytesIndex] = char(0); //ending zero byte

            return bytesLength;
            
        }
        case LOGIN: 
        {
            string username;
            string password;

            ssin >> username;
            ssin >> password;

            std::vector<char> userBytes (username.begin(), username.end());
            std::vector<char> passwordBytes (password.begin(), password.end());

            size_t bytesLength = 2 + userBytes.size() + 1 + passwordBytes.size() + 1; // op code, bytes for name, 0 byte, byte for password, 0 byte
            size_t bytesIndex = 0;

            short opCode = 2;

            shortToBytes(opCode,theBytes,0); //enter op code to first 2 bytes
            bytesIndex += 2;

            
            for(size_t i = 0; i < userBytes.size(); i++) //getting bytes for username
            {
                theBytes[bytesIndex] = userBytes.at(i);
                bytesIndex ++;
            }

            theBytes[bytesIndex] = char(0); //zero byte
            bytesIndex++;

            
            for(size_t i = 0; i < passwordBytes.size(); i++) //bytes for password
            {
                theBytes[bytesIndex] = passwordBytes.at(i);
                bytesIndex++;
            }

            theBytes[bytesIndex] = char(0); //ending zero byte

            return bytesLength;

        }

        case LOGOUT: 
        {
            short opcode = 3;

            shortToBytes(opcode,theBytes,0); //enter opcode to first 2 bytes
            size_t bytesLength = 2;
            return bytesLength;
        }

        case FOLLOW: 
        {
            short opcode = 4;
            size_t bytesLength = 2; //2 bytes for op code

            char willFollow;
            ssin >> willFollow; //one char if following or unfollowing
            bytesLength += 1; //1 byte for if following or unfollowing

            string numOfUsers;
            ssin >> numOfUsers; //string for num of users

            short numUsers = (short) std::stoi(numOfUsers); //num of users in short
            bytesLength +=2; //2 bytes for num of users short

            std::vector<std::vector<char>> userNames;
            string username;
            
            for(int i = 0; i < numUsers ; i++) //filling the userNames vector with vectors of chars of all the users to un/follow
            {
                ssin >> username;
                std::vector<char> userBytes(username.begin(), username.end());
                userNames.push_back(userBytes);

                bytesLength += userBytes.size(); //num of bytes for this username
            }
            bytesLength += numUsers; //bytes for zeros between names and ending zero


            size_t bytesIndex = 0;

            shortToBytes(opcode,theBytes,bytesIndex); //entering op code in first 2 bytes
            bytesIndex += 2;

            theBytes[bytesIndex] = willFollow; //entering single byte of willfollow
            bytesIndex += 1;

            shortToBytes(numUsers,theBytes,bytesIndex);
            bytesIndex += 2; //entering num of users bytes 

            
            for(size_t i = 0; i < userNames.size(); i++) //going through all the names
            {
                for(size_t j = 0; j < userNames.at(i).size(); j++) //going through the bytes of the name
                {
                    theBytes[bytesIndex] = userNames.at(i).at(j);
                    bytesIndex += 1;
                }
                theBytes[bytesIndex] = char(0);  //adds zero byte between each name and after the last name to close message
                bytesIndex += 1;
            }

            return bytesLength;
        }

        case POST: 
        {
            short opCode = 5;

            string content = toEncode.substr(5); //POST_startofcontent..  starting on index 5

            std::vector<char> contentBytes(content.begin(),content.end());
            size_t contentSize = content.size();
            
            size_t bytesLength = 2 + contentSize + 1;//2 for opcode, content size, and ending zero byte
            shortToBytes(opCode,theBytes,0);

            
            for(size_t i = 0; i < contentBytes.size(); i++)
            {
                theBytes[i + 2] = contentBytes.at(i);
            }

            theBytes[2 + contentSize] = char(0);

            return bytesLength;
        }

        case PM: 
        {
            string username;
            string content;

            ssin >> username;
            content = toEncode.substr(2 + username.length() + 1); //PM_userLength_startofcontents

            std::vector<char> userBytes (username.begin(), username.end());
            std::vector<char> contentBytes (content.begin(), content.end());

            size_t bytesLength = 2 + userBytes.size() + 1 + contentBytes.size() + 1; // op code, bytes for name, 0 byte, byte for content, 0 byte
            size_t bytesIndex = 0;

            short opCode = 6;

            shortToBytes(opCode,theBytes,0); //enter op code to first 2 bytes
            bytesIndex += 2;
            
            for(size_t i = 0; i < userBytes.size(); i++) //getting bytes for username
            {
                theBytes[bytesIndex] = userBytes.at(i);
                bytesIndex ++;
            }

            theBytes[bytesIndex] = char(0); //zero byte
            bytesIndex++;

            
            for(size_t i = 0; i < contentBytes.size(); i++) //bytes for password
            {
                theBytes[bytesIndex] = contentBytes.at(i);
                bytesIndex++;
            }

            theBytes[bytesIndex] = char(0); //ending zero byte

            return bytesLength;
        }

        case USERLIST: 
        {
            short opCode = 7;
            shortToBytes(opCode,theBytes,0); //entering the opcode

            size_t bytesLength = 2;
            return bytesLength;
        }

        case STAT: 
        {
            short opCode = 8;
            
            string username;
            ssin >> username;

            vector<char> userbytes(username.begin(),username.end());

            size_t bytesLength = 2 + userbytes.size() + 1;//2 for op code, then for userbytes, then ending zero byte

            shortToBytes(opCode,theBytes,0); //entering op code in first 2 bytes

            
            for(size_t i = 0; i < userbytes.size(); i++) //entering username bytes
            {
                theBytes[i +2] = userbytes.at(i);
            }

            theBytes[2 + userbytes.size()] = char(0); //entering final zero byte

            return bytesLength;
            
        }
    
    }
    return 0;

}



string_code BGSencdec::commandOf(string in){
    if (in == "REGISTER") return REGISTER;
    if (in == "LOGIN") return LOGIN;
    if (in == "LOGOUT") return LOGOUT;
    if (in == "FOLLOW") return FOLLOW;
    if (in == "POST") return POST;
    if (in == "PM") return PM;
    if (in == "USERLIST") return USERLIST;
    if (in == "STAT") return STAT;

    return REGISTER;
}

void BGSencdec::shortToBytes(short num, char* bytesArr,size_t index)
{
    bytesArr[index] = ((num >> 8) & 0xFF);
    bytesArr[index + 1] = (num & 0xFF);
}


BGSMessage* BGSencdec::decodeNextByte(char* byte){
    if (opCode != 0){  //allready working on a message with the current opcode, keep reading bytes to it
        switch(opCode){
            case 9:  //decoding notification message
            {
                return currentDecoder -> decodeNextByte(byte);
            }
            case 10: //decoding ack message
            {
                return currentDecoder -> decodeNextByte(byte);
            }
            case 11: //decoding error message
            {
                return currentDecoder -> decodeNextByte(byte);
            }
        }
    }
    if (!decodingMessage){ //not decoding message, getting first byte of opcode
        bytes.push_back(*byte);
        decodingMessage = true;
        return nullptr;
    }

    //op code does not exist and decoding message is true, so now getting the second byte for opcode
    bytes.push_back(*byte);
    opCode = bytesToShort(bytes);

    bytes.clear(); //clearing the bytes vector

    switch(opCode){
        case 9:
        {
            currentDecoder = new notifcDecoder();
            break;
        }
        case 10:
        {
            currentDecoder = new ackDecoder();
            break;
        }
        case 11:
        {
            currentDecoder = new errorDecoder();
            break;
        }
    };

    return nullptr;
}

short BGSencdec::bytesToShort(std::vector<char> bytesArr)
{
    short result = (short)((bytesArr.at(0) & 0xff) << 8);
    result += (short)(bytesArr.at(1) & 0xff);
    return result;
}

BGSencdec::~BGSencdec(){
    delete currentDecoder;
    currentDecoder = nullptr;
};

//#########      notifDecoder
notifcDecoder::notifcDecoder():gotType(false), type(-1), gotUser(false),username(),content(){
    
}

notifcDecoder::~notifcDecoder(){}


BGSMessage* notifcDecoder::decodeNextByte(char* byte){
    if (!gotType){ //first byte of message, wether following or not
        if (*byte == 0) type = '0';
        else if (*byte == 1) type = '1';
        gotType = true;
        return nullptr;
    }
    if(!gotUser){ //getting the user name
        if (*byte == 0){ //just finsihed reading username
            gotUser = true;
            return nullptr;
        }
        else { //still reading username
            username+=*byte;
            return nullptr;
        }
    }
    //getting the content       
    if (*byte == 0){ //just finished getting content. constructing and returning the message
        NotificationMessage* message = new NotificationMessage(type,username,content);

        return message;
    }
    //still reading content
    content += *byte;
    return nullptr;
}

//######## errorDecoder

errorDecoder::errorDecoder(): firstbyte(-1) {
    
}

errorDecoder::~errorDecoder(){}

BGSMessage* errorDecoder::decodeNextByte(char* byte){
    if (firstbyte == -1){ //now getting first byte
        firstbyte = *byte;
        return nullptr;
    }
    //now getting second byte
    std::vector<char> bytes;
    bytes.push_back(firstbyte);
    bytes.push_back(*byte);

    short messageOp = bytesToShort(bytes);

    ErrorMessage* message = new ErrorMessage(messageOp);
    return message;

}

//######## ackDecoder
ackDecoder::ackDecoder():firstbyte(-1), currentAckDecoder(nullptr),messageOp(-1){}

ackDecoder::~ackDecoder(){
    delete currentAckDecoder;
    currentAckDecoder = nullptr;
}

BGSMessage* ackDecoder::decodeNextByte(char* byte){
    if (messageOp != -1) {
        switch (messageOp) {
            case 4: //follow message
            {
                return currentAckDecoder->decodeNextByte(byte);
            }
            case 7: {
                return currentAckDecoder->decodeNextByte(byte);
            }
            case 8: {
                return currentAckDecoder->decodeNextByte(byte);
            }
        }
    }


    if (firstbyte == -1){ //getting first byte of the messageOp
        firstbyte = *byte;
        return nullptr;
    }
    //getting second byte of the messageOp
    std::vector<char> bytes;
    bytes.push_back(firstbyte);
    bytes.push_back(*byte);

    messageOp = bytesToShort(bytes);

    switch (messageOp){
        case 1: 
        {
            AckMessage* message = new AckMessage(messageOp);
            return message;
        }
        case 2:
        {
            AckMessage* message = new AckMessage(messageOp);
            return message;
        }
        case 3:
        {
            AckMessage* message = new AckMessage(messageOp);
            return message;
        }
        case 4: //follow message
        {
            currentAckDecoder = new FollowAckDecoder(4);
            return nullptr;
        }
        case 5:
        {
            AckMessage* message = new AckMessage(messageOp);
            return message;
        }
        case 6:
        {
            AckMessage* message = new AckMessage(messageOp);
            return message;
        }
        case 7: //userlist message
        {
            currentAckDecoder = new FollowAckDecoder(7);
            return nullptr;
        }
        case 8: //stat message
        {
            currentAckDecoder = new StatAckDecoder;
            return nullptr;
        }
    }
    return nullptr;
}

//######### StatAckDecoder
StatAckDecoder::StatAckDecoder(): numOfPosts(-1),numFollowers(-1),numFollowing(-1), bytes() {}

StatAckDecoder::~StatAckDecoder(){

}


BGSMessage* StatAckDecoder::decodeNextByte(char* byte){
    if (numOfPosts == -1){ //still didnt get num of posts
        if (bytes.size() == 0){ //did not get any byte yet
            bytes.push_back(*byte);
            return nullptr;
        }
        else{ //getting second byte for num of posts
            bytes.push_back(*byte);
            numOfPosts = bytesToShort(bytes);
            bytes.clear(); //clearing vector to use it again later
            return nullptr;
        }
    }
    if (numFollowers == -1){ //getting bytes for num of followers
        if (bytes.size() == 0){ //did not get any byte yet
            bytes.push_back(*byte);
            return nullptr;
        }
        else{ //getting second byte for num of followers
            bytes.push_back(*byte);
            numFollowers = bytesToShort(bytes);
            bytes.clear(); //clearing vector to use it again later
            return nullptr;
        }
    }
    if (numFollowing == -1){
        if (bytes.size() == 0){ //did not get any byte yet
            bytes.push_back(*byte);
            return nullptr;
        }
        else{ //getting second byte for num of followers
            bytes.push_back(*byte);
            numFollowing = bytesToShort(bytes);
            StatAckMessage* message = new StatAckMessage(numOfPosts,numFollowers,numFollowing);
            return message;
        }
    }
    return nullptr;
}



//############ FollowAckDecoder
FollowAckDecoder::FollowAckDecoder(int op):numOfUsers(-1),firstNumByte(-1), usernames(), username(""), counter(0),messageOp(op){}

FollowAckDecoder::~FollowAckDecoder(){}

BGSMessage* FollowAckDecoder::decodeNextByte(char* byte){
    if (numOfUsers == -1){ //didnt get num of users yet
        if (firstNumByte == -1){ //didnt get anything yet
            firstNumByte = *byte;
            return nullptr;
        }
        char secondbyte = *byte;

        std::vector<char> shortBytes;
        shortBytes.push_back(firstNumByte);
        shortBytes.push_back(secondbyte);

        numOfUsers = bytesToShort(shortBytes);
        counter = numOfUsers;
        username="";
        return nullptr;
    }
    //got num of users
    if (*byte == 0){ //end of user name or end of message
        counter--;
        usernames.push_back(username);
        if (counter == 0 ){ //done with this message
            UserlistOrFollowAckMessage* message = new UserlistOrFollowAckMessage(messageOp,numOfUsers,usernames);
            return message;
        }
        else { //still got more names to read
            username = ""; //clearing username to append new name
            return nullptr;
        }
    }
    // not zero byte, appending to name

    username += *byte;
    return nullptr;
}


package bgu.spl.net.impl.BGSServer.messages;

public class BGSMessage {
    private short opcode;

    public short getOpcode() {
        return opcode;
    }

    public BGSMessage(short opcode) {
        this.opcode = opcode;
    }
}

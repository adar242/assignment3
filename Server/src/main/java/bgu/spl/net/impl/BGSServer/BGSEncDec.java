package bgu.spl.net.impl.BGSServer;

import bgu.spl.net.api.MessageEncoderDecoder;
import bgu.spl.net.impl.BGSServer.messages.*;
import bgu.spl.net.impl.BGSServer.messages.ackMessages.*;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;

public class BGSEncDec implements MessageEncoderDecoder<BGSMessage> {
    protected short Opcode = 0;
    protected boolean decodingMessage = false;
    private ByteBuffer bytes;
    private MessageEncoderDecoder<? extends BGSMessage> currentDecoder;


    @Override
    public BGSMessage decodeNextByte(byte nextByte) {
        if (Opcode!=0) {   //opcode is known, calling the correct decode method to the specific opcode
            return currentDecoder.decodeNextByte(nextByte);
        }

        if (!decodingMessage) {  //starting to decode a new message, getting first byte for opcode
            bytes = ByteBuffer.allocate(2); //init new bites array sized 2, to get the 2 bytes that represent the opcode
            decodingMessage = true;
            bytes.put(nextByte);
            return null;
        }
        bytes.put(nextByte);  //this is the second time this was method was called for the message, resolving its opcode
        Opcode = bytesToShort(bytes.array()); // get the op code by converting given bytes to short


        // init the relevant current message and current decoder
        switch (Opcode){
            case 1 : currentDecoder = new RegisterDecoder(); break;
            case 2 : currentDecoder = new LoginDecoder(); break;
            //if op code is 3, then thats a logout message that dont need further decoding
            case 3 : decodingMessage = false; Opcode = 0; return new LogoutMessage();
            case 4 : currentDecoder = new FollowDecoder(); break;
            case 5 : currentDecoder = new PostDecoder(); break;
            case 6 : currentDecoder = new PMDecoder(); break;
            //if op code is 7, then thats a userlist message that dont need further decoding
            case 7 : decodingMessage = false; Opcode = 0; return new UserListMessage();
            case 8 : currentDecoder = new StatDecoder(); break;
        }

        return null;
    }


    @Override
    public byte[] encode(BGSMessage message) {
        short opcode = message.getOpcode();
        switch (opcode){
            case 9 : return encodeNotifMessage((NotificationMessage) message);
            case 10 : return encodeACKMessage((ACKMessage) message);
            case 11 : return encodeErrorMessage((ErrorMessage) message);
        }
        return null;
    }

    @Override
    public void reset() {
        Opcode = 0;
        decodingMessage = false;
        currentDecoder = null;
    }

    private byte[] encodeErrorMessage(ErrorMessage message) {
        ByteBuffer encodedMessage = ByteBuffer.allocate(4);

        encodedMessage.putShort(message.getOpcode());
        encodedMessage.putShort(message.getMessageOpcode());

        return encodedMessage.array();
    }

    private byte[] encodeACKMessage(ACKMessage message) {
        short opcode = message.getMessageOpcode();

        switch (opcode){
            case 1 : return encodeGeneralAck(message);
            case 2 : return encodeGeneralAck(message);
            case 3 : return encodeGeneralAck(message);
            case 4 : return encodeUserlistOrFollowAckMessage((UserlistOrFollowAckMessage)message);
            case 5 : return encodeGeneralAck(message);
            case 6 : return encodeGeneralAck(message);
            case 7 : return encodeUserlistOrFollowAckMessage((UserlistOrFollowAckMessage)message);
            case 8 : return encodeStatAckMessage((StatAckMessage)message);
        }
        return null;
    }

    private byte[] encodeStatAckMessage(StatAckMessage message) {
        ByteBuffer encodedMessage = ByteBuffer.allocate(10);

        encodedMessage.putShort(message.getOpcode());  //op code of general ack message
        encodedMessage.putShort((message.getMessageOpcode())); //op code of the message to be acknowledged
        encodedMessage.putShort(message.getNumOfPosts()); //num of posts the user posted
        encodedMessage.putShort(message.getNumOfFollowers()); //num of followers for this user
        encodedMessage.putShort(message.getNumOfFollowing()); //num of users this user follow
        encodedMessage.flip();
        return encodedMessage.array();

    }

    private byte[] encodeUserlistOrFollowAckMessage(UserlistOrFollowAckMessage message) {
        short numOfUsers = message.getNumOfUsers();
        ConcurrentLinkedQueue<String> userNameList = message.getUsernameList();
        byte zeroByte = 0;

        List<byte[]> userNamesInBytes = new ArrayList<>(); //list of the usernames in bytes
        int sizeOfUserListBytes = 0;  //counter of total bytes for all user names

        for (String userName : userNameList){
            byte[] userNameInBytes = userName.getBytes();
            sizeOfUserListBytes += userNameInBytes.length;
            userNamesInBytes.add(userNameInBytes);
        }

        sizeOfUserListBytes += numOfUsers - 1;  //adding the amount of zero bytes

        ByteBuffer encodedMessage = ByteBuffer.allocate(2 + 2 + 2 + sizeOfUserListBytes + 1);  //ack op, message op, numOfUsers, size of names, ending zero byte
        encodedMessage.putShort(message.getOpcode()); //adding opcode bytes
        encodedMessage.putShort(message.getMessageOpcode()); //adding acked message opcode bytes
        encodedMessage.putShort(numOfUsers); //adding numofusers bytes
        for (byte[] userNameInBytes : userNamesInBytes){
            encodedMessage.put(userNameInBytes);  //adding one of the names in bytes
            encodedMessage.put(zeroByte); //adding zero after the name (and also at the end of the array)
        }
        encodedMessage.flip();
        return encodedMessage.array();

    }

    private byte[] encodeGeneralAck(ACKMessage message) {
        ByteBuffer encodedMessage = ByteBuffer.allocate(4);
        encodedMessage.putShort(message.getOpcode());
        encodedMessage.putShort(message.getMessageOpcode());
        encodedMessage.flip();
        return encodedMessage.array();

    }


    private byte[] encodeNotifMessage(NotificationMessage message) {
        //getting bytes for opcode
        ByteBuffer opcodeBuffer = ByteBuffer.allocate(2);
        opcodeBuffer.putShort(message.getOpcode());
        opcodeBuffer.flip();
        //getting bytes for notification type
        ByteBuffer notifType = ByteBuffer.allocate(1);
        byte type = 1;
        if (message.getType() == 0) type = 0;
        notifType.put(type);
        notifType.flip();


        //getting bytes for the posting user string
        byte[] user = message.getPostingUser().getBytes();

        //getting bytes for the content string
        byte[] content = message.getContent().getBytes();

        byte zeroByte = 0;

        //assembling the encoded byte array
        ByteBuffer encodedMessage = ByteBuffer.allocate(2 + 1 + user.length + 1 + content.length + 1); //opbytes + type byte + userstring bytes + 0 byte + contentstring bytes + 0 byte
        encodedMessage.put(opcodeBuffer);
        encodedMessage.put(notifType);
        encodedMessage.put(user);
        encodedMessage.put(zeroByte);
        encodedMessage.put(content);
        encodedMessage.put(zeroByte);
        encodedMessage.flip();
        return encodedMessage.array();


    }

    public short bytesToShort(byte[] byteArr)
    {
        short result = (short)((byteArr[0] & 0xff) << 8);
        result += (short)(byteArr[1] & 0xff);
        return result;
    }


    private class RegisterDecoder implements MessageEncoderDecoder<RegisterMessage> {
        StringBuilder builder = new StringBuilder();
        boolean gotFirstZero = false;
        RegisterMessage message = new RegisterMessage();

        @Override
        public RegisterMessage decodeNextByte(byte nextByte) {
            if (nextByte == 0 && gotFirstZero) { //done with reading the message, builder now has the password
                message.setPassword(builder.toString());
                Opcode = 0; //set op code back to 0, to support decoding new messages
                decodingMessage = false;
                return message;
            }
            if (nextByte == 0) { //first zero, builder now has the username
                gotFirstZero = true;
                message.setUsername(builder.toString()); //set the username in the message
                builder = new StringBuilder(); //clearing the builder to start building a new string that will contain the password
                return null;
            }

            builder.append((char)nextByte); //appending the next byte to the string builder

            return null;
        }

        @Override
        public byte[] encode(RegisterMessage message) {
            return new byte[0];
        }

        @Override
        public void reset() {
            builder = new StringBuilder();
            gotFirstZero = false;
            message = new RegisterMessage();
        }
    }

    private class LoginDecoder implements MessageEncoderDecoder<LoginMessage> {
        private StringBuilder builder = new StringBuilder();
        private boolean gotFirstZero = false;
        private LoginMessage message = new LoginMessage();

        @Override
        public LoginMessage decodeNextByte(byte nextByte) {
            if (nextByte == 0 && gotFirstZero) { //done with reading the message, builder now has the password
                message.setPassword(builder.toString());
                Opcode = 0; //set op code back to 0, to support decoding new messages
                decodingMessage = false;
                return message;
            }
            if (nextByte == 0) { //first zero, builder now has the username
                gotFirstZero = true;
                message.setUsername(builder.toString()); //set the username in the message
                builder = new StringBuilder(); //clearing the builder to start building a new string that will contain the password
                return null;
            }

            builder.append((char)nextByte); //appending the next byte to the string builder

            return null;
        }

        @Override
        public byte[] encode(LoginMessage message) {
            return new byte[0];
        }

        @Override
        public void reset() {
            builder = new StringBuilder();
            gotFirstZero = false;
            message = new LoginMessage();
        }
    }


    private class FollowDecoder implements MessageEncoderDecoder<FollowMessage> {
        int byteCounter = 0;
        ByteBuffer numOfUsersInBytes = ByteBuffer.allocate(2);
        private short num_of_users_counter;
        FollowMessage message = new FollowMessage();
        StringBuilder builder = new StringBuilder();

        @Override
        public FollowMessage decodeNextByte(byte nextByte) {

            if (byteCounter == 0){ //now getting the first byte of message
                message.setShouldFollow((char)nextByte); //setting if should follow or unfollow
                byteCounter++;
                return null;
            }
            if (byteCounter == 1){ //getting first byte for num of users
                numOfUsersInBytes.put(nextByte);
                byteCounter++;
                return null;

            }
            if (byteCounter == 2) { //finishing getting num of users
                numOfUsersInBytes.put(nextByte);
                num_of_users_counter = bytesToShort(numOfUsersInBytes.array());
                message.setNumOfUsers(num_of_users_counter); //setting num of users
                byteCounter++;
                return null;
            }
            if (nextByte == 0){  //0 that separates names or in end of parameter
                if (num_of_users_counter == 1){  //this is the last zero, returning the message
                    message.setAndArrangeUserNameList(builder.toString());
                    return message;
                }
                else{ //not last zero, adding zero to string so could be split later
                    builder.append('0');
                    num_of_users_counter--;
                    return null;
                }
            }
            else{ //getting byte for the string
                builder.append((char)nextByte);
                return null;
            }
        }

        @Override
        public byte[] encode(FollowMessage message) {
            return new byte[0];
        }

        @Override
        public void reset() {
            byteCounter = 0;
            numOfUsersInBytes = ByteBuffer.allocate(2);
            message = new FollowMessage();
            builder = new StringBuilder();
        }
    }

    private class PostDecoder implements MessageEncoderDecoder<PostMessage> {
        PostMessage message = new PostMessage();
        StringBuilder builder = new StringBuilder();

        @Override
        public PostMessage decodeNextByte(byte nextByte) {
            if (nextByte == 0){ //finished building the string
                message.setContent(builder.toString());
                Opcode = 0;
                decodingMessage = false;
                return message;
            }
            builder.append((char)nextByte);
            return null;
        }

        @Override
        public byte[] encode(PostMessage message) {
            return new byte[0];
        }

        @Override
        public void reset() {
            message = new PostMessage();
            builder = new StringBuilder();
        }
    }

    private class PMDecoder implements MessageEncoderDecoder<PMMessage> {
        PMMessage message = new PMMessage();
        StringBuilder builder = new StringBuilder();
        boolean gotUserName = false;

        @Override
        public PMMessage decodeNextByte(byte nextByte) {
            if (!gotUserName){
                if (nextByte == 0) { //done with user name
                    message.setUsername(builder.toString());
                    gotUserName = true;
                    builder = new StringBuilder(); //init new builder for content string
                    return null;
                }
                //not done with user name, keep building it
                builder.append((char)nextByte);
                return null;
            }

            //done with username, building content string
            if (nextByte == 0){  //done with content string
                message.setContent(builder.toString());
                Opcode = 0;
                decodingMessage = false;
                return message;
            }

            //not done with content string, keep building it
            builder.append((char)nextByte);
            return null;
        }

        @Override
        public byte[] encode(PMMessage message) {
            return new byte[0];
        }

        @Override
        public void reset() {
            message = new PMMessage();
            builder = new StringBuilder();
            gotUserName = false;
        }
    }

    private class StatDecoder implements MessageEncoderDecoder<StatMessage> {
        StringBuilder builder = new StringBuilder();
        StatMessage message = new StatMessage();

        @Override
        public StatMessage decodeNextByte(byte nextByte) {
            if (nextByte == 0){ //done building username string
                message.setUsername(builder.toString());
                Opcode = 0;
                decodingMessage = false;
                return message;
            }

            //not done building the string
            builder.append((char)nextByte);
            return null;
        }

        @Override
        public byte[] encode(StatMessage message) {
            return new byte[0];
        }

        @Override
        public void reset() {
            builder = new StringBuilder();
            message = new StatMessage();
        }
    }
}


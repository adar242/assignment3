package bgu.spl.net.impl.BGSServer.messages.ackMessages;

import bgu.spl.net.impl.BGSServer.messages.ACKMessage;

import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;

public class UserlistOrFollowAckMessage extends ACKMessage {

    private short numOfUsers;
    private ConcurrentLinkedQueue<String> usernameList;

    public UserlistOrFollowAckMessage(short messageOpcode, short numOfUsers, ConcurrentLinkedQueue<String> usernameList) {
        super(messageOpcode);
        this.numOfUsers = numOfUsers;
        this.usernameList = usernameList;
    }

    public short getNumOfUsers() {
        return numOfUsers;
    }

    public ConcurrentLinkedQueue<String> getUsernameList() {
        return usernameList;
    }
}

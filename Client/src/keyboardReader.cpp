#include <keyboardReader.h>
#include <thread>
#include <connectionHandler.h>
#include <keyboardReader.h>


keyboardReader::keyboardReader(ConnectionHandler* someHandler,BGSencdec* encdec):myHandler(someHandler),myEncdec(encdec) {
}

//the method that will run in its own thread. getting line from command prompt and sending an encoded version of it
void keyboardReader::run(){
    while (1){
        const short bufsize = 1024;
        char buf[bufsize];
        std::cin.getline(buf, bufsize);
        std::string line(buf);
        char* bytes = new char[10000];
        size_t bytesLength = myEncdec -> encode(line,bytes);
        
        if (bytesLength!=0) { //there is a message to send
            if(!myHandler -> sendBytes(bytes,bytesLength)){
                std::cout << "Disconnected. Exiting...\n" << std::endl;
                break;
            }
        }
    }
}




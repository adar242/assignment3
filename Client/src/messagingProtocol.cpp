#include <messagingProtocol.h>
#include <iostream>

using std::string;
using std::cout;
using std::endl;

MessagingProtocol::MessagingProtocol():isTerminated(false){

};


void MessagingProtocol::process(BGSMessage& message) {
    switch (message.getOpcode()) { //checks which kind of message it is
        case 9 : // notification message
        notificationProcess(dynamic_cast<NotificationMessage &>(message));
            break;
        case 10 : // ack message
            ackProcess(dynamic_cast<AckMessage &>(message));
            break;
        case 11: // error messages
            errorProcess(dynamic_cast<ErrorMessage &>(message));
            break;
    }
}

void MessagingProtocol::notificationProcess(NotificationMessage& message) {
    std::string type;
    if (message.getType() == '0') {
        type = "PM";
    } else {
        type = "Public";
    }
    std::cout << "NOTIFICATION " + type + " " + message.getPostingUser() + " " + message.getContent() << std::endl;
}

void MessagingProtocol::errorProcess(ErrorMessage& message) {
    std::cout << "ERROR "+ std::to_string(message.getMessageOpcode()) << std::endl;
}

void MessagingProtocol::ackProcess(AckMessage& message) {
    switch (message.getMessageOpcode()) { //checks which ack message it is
        case 8: // stat ack message
            statAckProcess(dynamic_cast<StatAckMessage &>(message));
            break;
        case 4: // follow ack message
            userlistOrFollowAckProcess(dynamic_cast<UserlistOrFollowAckMessage &>(message));
            break;
        case 7: // userlist ack message
            userlistOrFollowAckProcess(dynamic_cast<UserlistOrFollowAckMessage &>(message));
            break;
        default: // different ack message
        std::cout << "ACK " + std::to_string(message.getMessageOpcode()) << std::endl;
        if (message.getMessageOpcode() == 3){
            this -> isTerminated = true;
            std::cout<<"should now terminate"<<std::endl;
        }
    }

}

void MessagingProtocol::statAckProcess(StatAckMessage& message) {
    std::cout << "ACK " + std::to_string(message.getMessageOpcode()) + " " + std::to_string(message.getNumOfPosts())
    + " " + std::to_string(message.getNumOfFollowers()) + " " + std::to_string(message.getNumOfFollowing()) << std::endl;
}

void MessagingProtocol::userlistOrFollowAckProcess(UserlistOrFollowAckMessage& message) {
    std::string usernameVector;
    for (int unsigned i = 0; i < message.getUsernameVector().size(); i++) { // organizes the list in printing form
        usernameVector.append(message.getUsernameVector()[i] + " ");
    }
    std::cout << "ACK " <<(message.getMessageOpcode()) <<" "<< std::to_string(message.getNumOfUsers())<<" "
    << usernameVector << std::endl;
}

bool MessagingProtocol::shouldTerminate() {
    return isTerminated;
}



package bgu.spl.net.impl.BGSServer.messages;

public class LogoutMessage extends BGSMessage {

    public LogoutMessage() {
        super((short)3);
    }
}

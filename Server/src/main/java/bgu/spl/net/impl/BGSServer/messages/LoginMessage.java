package bgu.spl.net.impl.BGSServer.messages;

public class LoginMessage extends BGSMessage {

    private String username;
    private String password;

    public LoginMessage() {
        super((short)2);
        username = null;
        password = null;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}

package bgu.spl.net.impl.BGSServer.messages;

public class NotificationMessage extends BGSMessage {

    private char type;
    private String postingUser;
    private String content;

    public NotificationMessage(char type, String postingUser, String content) {
        super((short)9);
        this.type = type;
        this.postingUser = postingUser;
        this.content = content;
    }

    public char getType() {
        return type;
    }

    public String getPostingUser() {
        return postingUser;
    }

    public String getContent() {
        return content;
    }

    public void setType(char type) {
        this.type = type;
    }

    public void setPostingUser(String postingUser) {
        this.postingUser = postingUser;
    }

    public void setContent(String content) {
        this.content = content;
    }
}

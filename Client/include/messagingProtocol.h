#ifndef BOOST_ECHO_CLIENT_MESSAGINGPROTOCOL_H
#define BOOST_ECHO_CLIENT_MESSAGINGPROTOCOL_H

#include <string>
#include "BGSMessage.h"

class MessagingProtocol {

public:

    MessagingProtocol();

    void process(BGSMessage& message);

    void notificationProcess(NotificationMessage& message);

    void errorProcess(ErrorMessage& message);

    void ackProcess(AckMessage& message);

    void statAckProcess(StatAckMessage& message);

    void userlistOrFollowAckProcess(UserlistOrFollowAckMessage& message);

    bool shouldTerminate();


private:
    bool isTerminated;

};
#endif

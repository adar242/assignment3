#ifndef KEYBOARD_READER
#define KEYBOARD_READER

#include <connectionHandler.h>
#include <BGSencdec.h>

class keyboardReader {
private:
    ConnectionHandler* myHandler;
    BGSencdec* myEncdec;

public:
    keyboardReader(ConnectionHandler*,BGSencdec*);


    void run();


};
#endif

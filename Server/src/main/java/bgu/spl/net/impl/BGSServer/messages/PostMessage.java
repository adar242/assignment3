package bgu.spl.net.impl.BGSServer.messages;

public class PostMessage extends BGSMessage {

    private String content;

    public PostMessage() {
        super((short)5);
        content = null;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}

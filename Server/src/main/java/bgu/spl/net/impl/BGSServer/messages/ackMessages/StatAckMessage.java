package bgu.spl.net.impl.BGSServer.messages.ackMessages;

import bgu.spl.net.impl.BGSServer.messages.ACKMessage;

public class StatAckMessage extends ACKMessage {

    private short numOfPosts;
    private short numOfFollowers;
    private short numOfFollowing;

    public StatAckMessage(short messageOpcode, short numOfPosts, short numOfFollowers, short numOfFollowing) {
        super(messageOpcode);
        this.numOfPosts = numOfPosts;
        this.numOfFollowers = numOfFollowers;
        this.numOfFollowing = numOfFollowing;
    }

    public short getNumOfPosts() {
        return numOfPosts;
    }

    public void setNumOfPosts(short numOfPosts) {
        this.numOfPosts = numOfPosts;
    }

    public short getNumOfFollowers() {
        return numOfFollowers;
    }

    public void setNumOfFollowers(short numOfFollowers) {
        this.numOfFollowers = numOfFollowers;
    }

    public short getNumOfFollowing() {
        return numOfFollowing;
    }

    public void setNumOfFollowing(short numOfFollowing) {
        this.numOfFollowing = numOfFollowing;
    }
}

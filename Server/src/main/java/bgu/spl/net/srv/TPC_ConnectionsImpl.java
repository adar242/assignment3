package bgu.spl.net.srv;

import bgu.spl.net.api.bidi.Connections;
import bgu.spl.net.srv.bidi.ConnectionHandler;

import java.util.HashMap;
import java.util.List;

public class TPC_ConnectionsImpl<T> implements Connections<T> {
    private HashMap<Integer,ConnectionHandler> connectionHandlerHashMap; // mapping the list so that every handler has a unique id
    private int nextId;

    public TPC_ConnectionsImpl() {
        connectionHandlerHashMap = new HashMap<>();
        nextId = 0;
    }

    public TPC_ConnectionsImpl(List<ConnectionHandler> connectionHandlerList) {
        for (int i = 0; i < connectionHandlerList.size(); i++) {
            connectionHandlerHashMap.put(i,connectionHandlerList.get(i)); // puts every handler in the map according to its original index
            nextId++;
        }
    }

    @Override
    public boolean send(int connectionId, T msg) { // returns whether the msg was sent to the relevant client
        if (connectionHandlerHashMap.containsKey(connectionId)) {
            ConnectionHandler relevantHandler = connectionHandlerHashMap.get(connectionId);
            relevantHandler.send(msg);
            return true;
        }
        return false; // there is no handler in the given connectionId
    }

    @Override
    public void broadcast(T msg) {
        for (Integer connectionId : connectionHandlerHashMap.keySet()) {
            connectionHandlerHashMap.get(connectionId).send(msg);
        }
    }

    @Override
    public void disconnect(int connectionId) {
        boolean isInMap = connectionHandlerHashMap.containsKey(connectionId);
        if (isInMap) { // checks if the specific handler is connected

            connectionHandlerHashMap.remove(connectionId);
        }
    }

    public int addHandler(ConnectionHandler handler) {
        boolean isInMap = connectionHandlerHashMap.containsValue(handler);
        if (!isInMap) { // checks if the handler is not already in the HashMap
            connectionHandlerHashMap.put(nextId,handler);
            nextId++;
           return nextId-1;
        }
        return -1;
    }

}

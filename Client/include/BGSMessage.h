#ifndef BOOST_ECHO_CLIENT_BGSMESSAGE_H
#define BOOST_ECHO_CLIENT_BGSMESSAGE_H

#include <string>
#include <vector>

using std::string;
using std::vector;

class BGSMessage {

private:
    short opcode;

public:
    BGSMessage(short opcode);

    virtual ~BGSMessage();

    virtual short getOpcode() const;

};

class ErrorMessage : public BGSMessage {

private:
    short messageOpcode;

public:
    ErrorMessage(short messageOpcode);

    virtual ~ErrorMessage();

    short getMessageOpcode() const;

};

class NotificationMessage : public BGSMessage {

private:
    char type;
    std::string postingUser;
    std::string content;

public:
    NotificationMessage(char type, const string &postingUser, const string &content);

    virtual ~NotificationMessage();

    char getType() const;

    const string &getPostingUser() const ;

    const string &getContent() const;
};

class AckMessage : public BGSMessage {

private:
    short messageOpcode;

public:
    AckMessage(short messageOpcode);

    virtual ~AckMessage();

    short getMessageOpcode() const;
};

class StatAckMessage : public AckMessage {

private:
    short numOfPosts;
    short numOfFollowers;
    short numOfFollowing;

public:
    StatAckMessage(short numOfPosts, short numOfFollowers, short numOfFollowing);

    virtual ~StatAckMessage();

    short getNumOfPosts() const;

    short getNumOfFollowers() const;

    short getNumOfFollowing() const;
};

class UserlistOrFollowAckMessage : public AckMessage {

private:
    short numOfUsers;
    std::vector<std::string> usernameVector;

public:
    UserlistOrFollowAckMessage(short messageOpcode, short numOfUsers, const vector<string> &usernameVector);

    virtual ~UserlistOrFollowAckMessage();

    short getNumOfUsers() const;

    const vector<string> &getUsernameVector() const;
};
#endif
